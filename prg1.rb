#1. Write a method that counts down to zero using recursion.

def countdown(n)
  if n < 0
    return
  end
  puts n
  countdown(n - 1)
end

puts "Enter the value to count down"
val = gets.chomp.to_i
puts "The countdown is:"
countdown(val)
