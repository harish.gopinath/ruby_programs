even_array = []
odd_array = []

for i in 1...101
  if i.even?
    even_array << i
  else
    odd_array << i
  end
end

def filter_divisible(numbers, divisors)
  divisible_numbers = []
  numbers.each do |num|
    divisors.each do |divisor|
      if num % divisor == 0
        divisible_numbers << num
        break
      end
    end
  end
  divisible_numbers
end

puts "The even array is:"
print even_array
puts ""
puts "-------------------"
puts "The odd array is:"
print odd_array

divisors = [4, 6, 8, 10, 3, 5, 7, 9]

even_divisible = filter_divisible(even_array, divisors)
odd_divisible = filter_divisible(odd_array, divisors)

puts ""
puts "Numbers divisible by #{divisors} from even array:"
print even_divisible
puts ""
puts "------------------------------"
puts "Numbers divisible by #{divisors} from odd array:"
print odd_divisible
