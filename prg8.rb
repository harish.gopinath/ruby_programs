# ake 20 integer inputs from user and print the following:
#     number of positive numbers
#     number of negative numbers
#     number of odd numbers
#     number of even numbers
#     number of 0s.


puts "Enter 20 integer numbers:"

arr = []

20.times do |i|
  number = gets.chomp.to_i
  arr << number
end

if arr.length != 20
  puts "Please enter exactly 20 integer numbers."
else
  negative_count = 0
  positive_count = 0
  even_count = 0
  odd_count = 0

  arr.each do |num|
    if num < 0
      negative_count += 1
    elsif num > 0
      positive_count += 1
    end

    if num.even?
      even_count += 1
    else
      odd_count += 1
    end
  end

  puts "The negative count is: #{negative_count}"
  puts "------------------------"
  puts "The positive count is: #{positive_count}"
  puts "------------------------"
  puts "The even count is: #{even_count}"
  puts "------------------------"
  puts "The odd count is: #{odd_count}"
end
