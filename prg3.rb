# Write a program, that asks a user how old they are and then tells
# them how old they will be in 10, 20, 30 and 40 years.
 def after_yars(age)
  puts "The age after 10 years is : #{age + 10}"
  puts "The age after 20 years is : #{age + 20}"
  puts "The age after 30 years is : #{age + 30}"
  puts "The age after 40 years is : #{age + 40}"
 end

 puts " Enter your current age"
 age=gets.chomp.to_i
 after_yars(age)
