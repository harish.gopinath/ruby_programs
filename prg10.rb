array = ["no bun","bug bun bug bun bug bug","bunny bug","buggy bug bug buggy"]

puts "Enter a string:"
input_string = gets.chomp

occurrences = array.map { |element| element.count(input_string) }

rearranged_array = array.zip(occurrences).sort_by { |_, count| -count }.map(&:first)

puts "Rearranged array based on occurrences of '#{input_string}':"
puts rearranged_array.join(", ")
