#Take the following array:
#a = ['white snow', 'winter wonderland', 'melting ice', 'slippery sidewalk', 'salted roads', 'white trees']
#and turn it into a new array that consists of strings containing one word. (ex. ["white snow", etc...] → ["white", "snow", etc...].

def split_on_whitespace(input)
  split_words = input.map { |phrase| phrase.split }.flatten
  print split_words
end
a = ['white snow', 'winter wonderland', 'melting ice', 'slippery sidewalk', 'salted roads', 'white trees']
split_on_whitespace(a)
