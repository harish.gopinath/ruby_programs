#  WAP to delete all of the strings that begin with an "s" and "w" in the following array.

def delete_strings_starting_with_sw(arr)
  arr.reject! { |str| str.start_with?('s', 'w') }
end

arr = ['snow', 'winter', 'ice', 'slippery', 'salted roads', 'white trees']
delete_strings_starting_with_sw(arr)

puts arr

