array_values = []

loop do
  puts " Enter the integer number and press q for exit"
  value = gets.chomp
  if (value == 'q')
    break
  else
  array_values << value.to_f
  end
end

def average(array_values)
  sum=array_values.sum
  size_array = array_values.size
  if size_array == 0
    puts "No numbers were entered."
  else
    average = sum.to_f / size_array
    puts "The average is #{average}"
  end
end

def product(array_values)
  if array_values.empty?
    puts "No numbers were entered."
  else
    product = array_values.inject(1) { |prod, num| prod * num }
    puts "The product of the numbers is #{product}"
  end
end

average(array_values)
product(array_values)
