class Time
  attr_accessor :hours, :minutes

  def initialize(hours, minutes)
    @hours = hours
    @minutes = minutes
  end

  def addTime(other_time)
    total_minutes = self.to_minutes + other_time.to_minutes
    new_hours = total_minutes / 60
    new_minutes = total_minutes % 60
    Time.new(new_hours, new_minutes)
  end

  def displayTime
    puts "The time is #{@hours} hour(s) and #{@minutes} minute(s)"
  end

  def displayMinutes
    total_minutes = self.to_minutes
    puts "The total minutes are #{total_minutes} minute(s)"
  end

  protected

  def to_minutes
    @hours * 60 + @minutes
  end
end

time1 = Time.new(2, 45)
time2 = Time.new(1, 20)

added_time = time1.addTime(time2)
added_time.displayTime

time1.displayMinutes
time2.displayMinutes
added_time.displayMinutes
