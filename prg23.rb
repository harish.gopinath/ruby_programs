def factorial_recursive(n)
  return 1 if n == 0
  n * factorial_recursive(n - 1)
end

puts "Enter a number to calculate its factorial:"
number = gets.chomp.to_i
puts "Factorial of #{number} (recursive) is: #{factorial_recursive(number)}"
