even_array = []
odd_array = []

for i in 1...101
  if i.even?
    even_array << i
  else
    odd_array << i
  end
end

puts "The even array is:"
puts even_array
puts "-------------------"
puts "The odd array is:"
puts odd_array
