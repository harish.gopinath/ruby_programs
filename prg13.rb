def remove_consonants(str)
  vowels = "aeiouAEIOU"
  str.delete("^#{vowels}")
end

input_string = "Hello, have a good day"
result_string = remove_consonants(input_string)
puts "Original string: #{input_string}"
puts "String with consonants removed: #{result_string}"
