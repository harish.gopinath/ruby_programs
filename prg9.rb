# Given an array of N integers, and an integer K, find the number of pairs of elements
# in the array whose sum is equal to K.

def count_pairs_with_sum(arr, k)
  count = 0
  arr.each_with_index do |num1, i|
    arr[i+1..-1].each do |num2|
      # Check if the sum of the pair equals k
      count += 1 if num1 + num2 == k
    end
  end
  count
end

arr = [1, 2, 3, 4, 5]
k = 6
puts "Number of pairs with sum #{k}: #{count_pairs_with_sum(arr, k)}"
