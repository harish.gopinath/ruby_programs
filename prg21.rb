word = "MISSISSIPPI"
letter_count = Hash.new(0)

word.each_char do |char|
  letter_count[char] += 1
end

sorted_letter_count = letter_count.sort_by { |letter, count| -count }

sorted_letter_count.each do |letter, count|
  puts "#{letter}: #{count}"
end
