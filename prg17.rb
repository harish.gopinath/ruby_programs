mixed_array = [1, 2.5, "hello", 3, "world", 4.2, 5, "foo", "bar", 6.8]

integers = []
strings = []
floats = []

mixed_array.each do |element|

  case element
  when Integer
    integers << element
  when Float
    floats << element
  when String
    strings << element
  end
end

puts "Integers:"
puts integers.inspect
puts "Strings:"
puts strings.inspect
puts "Floats:"
puts floats.inspect
