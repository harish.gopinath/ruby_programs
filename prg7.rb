# Suppose you have a hash h = { a:1, b:2, c:3, d:4, e:6 }
#     1. Get the value of key `:b`.
#     2. Add to this hash the key:value pair `{f:5}`
#     3. Remove all key:value pairs whose value is less than 5.5


h={a:1, b:2, c:3, d:4, e:6}
h = {a: 1, b: 2, c: 3, d: 4, e: 6}

puts "The value of key B is:"

if h.key?(:b)
  puts h[:b]
end

puts "Now adding the new key value pair and value is: "
h[:f] = 5
puts h

puts "Removing all key:value pairs whose value is less than 5.5"

h.delete_if { |key, value| value < 5.5 }

puts "After removing key-value pairs: "
puts h

