# Given an array with integer values you need to find all the duplicated numbers.


def find_duplicates(array)
  duplicates = []
  array.each do |element|
    duplicates << element if array.count(element) > 1
  end
  puts duplicates.uniq
end

arr=[1,2,3,4,4,5,5,5,5,5,6,7]
find_duplicates(arr)
